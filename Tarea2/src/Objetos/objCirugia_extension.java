/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author TIENDA 424
 */
public class objCirugia_extension extends objCirugias {
    private String clasificacion;

    public objCirugia_extension(String clasificacion, String nombre, String riesgo, String abordaje, String anestesia, String periodo_post, String rapidez) {
        super(nombre, riesgo, abordaje, anestesia, periodo_post, rapidez);
        this.clasificacion = clasificacion;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public static ArrayList getListaCirugias() {
        return listaCirugias;
    }

    public static void setListaCirugias(ArrayList listaCirugias) {
        objCirugias.listaCirugias = listaCirugias;
    }

    
}
