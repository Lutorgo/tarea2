/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

import Negocio.*;
import Datos.BD;
import java.util.ArrayList;

/**
 *
 * @author TIENDA 424
 */
public class objCirugias {
    private String nombre;
    private String riesgo;
    private String abordaje;
    private String anestesia;
    private String periodo_post;
    private String rapidez;

    public static ArrayList listaCirugias = new ArrayList<>();

    public objCirugias(String nombre, String riesgo, String abordaje, String anestesia, String periodo_post, String rapidez) {
        this.nombre = nombre;
        this.riesgo = riesgo;
        this.abordaje = abordaje;
        this.anestesia = anestesia;
        this.periodo_post = periodo_post;
        this.rapidez = rapidez;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRiesgo() {
        return riesgo;
    }

    public void setRiesgo(String riesgo) {
        this.riesgo = riesgo;
    }

    public String getAbordaje() {
        return abordaje;
    }

    public void setAbordaje(String abordaje) {
        this.abordaje = abordaje;
    }

    public String getAnestesia() {
        return anestesia;
    }

    public void setAnestesia(String anestesia) {
        this.anestesia = anestesia;
    }

    public String getPeriodo_post() {
        return periodo_post;
    }

    public void setPeriodo_post(String periodo_post) {
        this.periodo_post = periodo_post;
    }

    public String getRapidez() {
        return rapidez;
    }

    public void setRapidez(String rapidez) {
        this.rapidez = rapidez;
    }

    public static ArrayList getListaCirugias() {
        return listaCirugias;
    }

    public static void setListaCirugias(ArrayList listaCirugias) {
        objCirugias.listaCirugias = listaCirugias;
    }
    
    
}
