/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Datos.BD;
import Objetos.objCirugia_extension;
import Objetos.objCirugia_local_prof;
import java.util.ArrayList;

/**
 *
 * @author TIENDA 424
 */
public class Cirugia_local_prof {

    BD cirugia = new BD();

    public String[] devuelveDatosCirugia_Ex() {
        String tipo = "Externa";
        String tipo2 = "Interna";

        String[] datos = new String[1];

        datos[0] = tipo;
        datos[1] = tipo2;

        return datos;
    }

    public void insertarCirugias(ArrayList<objCirugia_local_prof> listaLocal) {
        String datos = "";
        for (int i = 0; i < listaLocal.size(); i++) {
            String clasificacion = listaLocal.get(i).getClasificacion();
            String riesgo = listaLocal.get(i).getRiesgo();
            String abordaje = listaLocal.get(i).getAbordaje();
            String anestesia = listaLocal.get(i).getAnestesia();
            String periodo = listaLocal.get(i).getPeriodo_post();
            String nombre = listaLocal.get(i).getNombre();
            datos = nombre + "," +clasificacion + "," + riesgo + "," + abordaje + "," + anestesia + "," + periodo;
            cirugia.insertarEnArchivo(datos);
        }
    }
}
