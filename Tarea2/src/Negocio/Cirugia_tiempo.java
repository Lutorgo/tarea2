/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Datos.BD;
import Objetos.objCirugia_obj_propos;
import Objetos.objCirugia_tiempo;
import java.util.ArrayList;

/**
 *
 * @author TIENDA 424
 */
public class Cirugia_tiempo {
    BD cirugia = new BD();

    public String[] devuelveDatosCirugia_Ex() {
        String tipo = "Emergencia";
        String tipo2 = "Urgencia";
        String tipo3 = "Electiva";

        String[] datos = new String[1];

        datos[0] = tipo;
        datos[1] = tipo2;
        datos[2]= tipo3;

        return datos;
    }

    public void insertarCirugias(ArrayList<objCirugia_tiempo> listaTiempo) {
        String datos = "";
        for (int i = 0; i < listaTiempo.size(); i++) {
            String clasificacion = listaTiempo.get(i).getClasificacion();
            String riesgo = listaTiempo.get(i).getRiesgo();
            String abordaje = listaTiempo.get(i).getAbordaje();
            String anestesia = listaTiempo.get(i).getAnestesia();
            String periodo = listaTiempo.get(i).getPeriodo_post();
            String nombre = listaTiempo.get(i).getNombre();
            datos = nombre + "," +clasificacion + "," + riesgo + "," + abordaje + "," + anestesia + "," + periodo;
            cirugia.insertarEnArchivo(datos);
        }
    }
}
