/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Datos.BD;
import Objetos.objCirugia_local_prof;
import Objetos.objCirugia_obj_propos;
import java.util.ArrayList;

/**
 *
 * @author TIENDA 424
 */
public class Cirugia_obj_propos {
    BD cirugia = new BD();

    public String[] devuelveDatosCirugia_Ex() {
        String tipo = "Curativa";
        String tipo2 = "Reparadora";
        String tipo3 = "Paliativa";
        String tipo4 = "Estética";

        String[] datos = new String[1];

        datos[0] = tipo;
        datos[1] = tipo2;
        datos[2] = tipo3;
        datos[3] = tipo4;

        return datos;
    }

    public void insertarCirugias(ArrayList<objCirugia_obj_propos> listaObjPropos) {
        String datos = "";
        for (int i = 0; i < listaObjPropos.size(); i++) {
            
            String clasificacion = listaObjPropos.get(i).getClasificacion();
            String riesgo = listaObjPropos.get(i).getRiesgo();
            String abordaje = listaObjPropos.get(i).getAbordaje();
            String anestesia = listaObjPropos.get(i).getAnestesia();
            String periodo = listaObjPropos.get(i).getPeriodo_post();
            String nombre = listaObjPropos.get(i).getNombre();
            datos = nombre + "," +clasificacion + "," + riesgo + "," + abordaje + "," + anestesia + "," + periodo;
            cirugia.insertarEnArchivo(datos);
        }
    }
}
