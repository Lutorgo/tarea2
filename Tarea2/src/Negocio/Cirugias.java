/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Datos.BD;
import java.util.ArrayList;

/**
 *
 * @author TIENDA 424
 */
public class Cirugias {
    BD cirugias = new BD();
    
     public ArrayList<String> LeerCirugias() {
        ArrayList<String> lista = cirugias.LeerDesdeArchivo();
        return lista;
    }

    public ArrayList<String> ModificarCirugias() {
        ArrayList<String> lista = cirugias.ModificarDatosArchivo();
        return lista;
    }

    public ArrayList<String> EliminarCirugias() {
        ArrayList<String> lista = cirugias.EliminarDatosArchivo();
        return lista;
    }
    
    public ArrayList<String> ActualizarCirugias() {
        ArrayList<String> lista = cirugias.ActualizarDatosArchivo();
        return lista;
    }
    
}
