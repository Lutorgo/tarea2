**Apertura:** martes, 31 de enero de 2023, 20:30
**Cierre:** jueves, 2 de febrero de 2023, 17:00

## Información sobre la tarea.

##Instrucciones.

El documento adjunto contiene el enunciado de lo que debe resolver.
Debe crear un único proyecto que contenga lo que se le solicita, puede crear una clase para cada uno.
Todo el proyecto debe estar en un repositorio de GitLab.
Entrega.

Debe subir por este mismo medio el proyecto en un zip, además, dentro del mismo zip debe incluir un documento de texto con el URL de GitLab donde ubicó el proyecto.
La fecha y hora límite de entrega es el jueves 02 de febrero a las 05:00 PM.
Evaluación.

La entrega esta relacionada al porcentaje del Trabajo en Clase correspondiente al curso.
Los estudiantes realizan una presentación oral de lo resuelto, de acuerdo con las indicaciones que el docente considere.
